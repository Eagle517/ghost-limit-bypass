#include <windows.h>

#include "TSHooks.hpp"

ADDR gNetInterface__processServer_finishedInitialGhostLoc1;
ADDR gNetInterface__processServer_finishedInitialGhostLoc2;

bool init()
{
	BlInit;

	BlScanHex(gNetInterface__processServer_finishedInitialGhostLoc1, "75 28 EB 03");
	BlScanHex(gNetInterface__processServer_finishedInitialGhostLoc2, "74 DD 8B B6 ? ? ? ?");

	BlPatchBytes(2, gNetInterface__processServer_finishedInitialGhostLoc1, "\x90\x90");
	BlPatchByte(gNetInterface__processServer_finishedInitialGhostLoc2, '\xEB');

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");
	return true;
}

bool deinit()
{
	BlPatchBytes(2, gNetInterface__processServer_finishedInitialGhostLoc1, "\x75\x28");
	BlPatchByte(gNetInterface__processServer_finishedInitialGhostLoc2, '\x74');

	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch(reason)
	{
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
