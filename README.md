# Ghost Limit Bypass
A DLL that removes the limit of how many ghost updates can happen per network tick.

Credit to [Pah1023](https://github.com/Pah1023) for the original version.

## Usage
The DLL will work as soon as it is injected.  Eject it to return behavior to normal.
